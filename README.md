## [XRF engine](https://github.com/xray-forge/stalker-xrf-engine)

Part of [XRF engine](https://github.com/xray-forge/stalker-xrf-engine) building pipeline.

## Links

- Forge gitlab: https://gitlab.com/xray-forge
- Forge github: https://github.com/xray-forge
- Bin: https://github.com/xray-forge/stalker-xrf-bin
- Engine: https://github.com/xray-forge/stalker-xrf-engine
- Types: https://github.com/xray-forge/xray-16-types
- Resources base: https://gitlab.com/xray-forge/stalker-xrf-resources-base
- Resources extended: https://gitlab.com/xray-forge/stalker-xrf-resources-extended
